import java.io.IOException;
import java.io.DataOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

public class MultiThreadServer2 {

   public static void main(String args[]) throws Exception {
//      ServerSocket ssock = new ServerSocket(1234);
//      System.out.println("Listening");
      int port = 1234;

		ServerSocket ssock = new ServerSocket(port);

      //listeningSocket listener = new listeningSocket(port);
      System.out.println("Listenning thread started");
      new Thread(new listeningSocket( ssock )).start();

		int count = 0;

      while (true) {
         System.out.println("Main server running...");
         TimeUnit.SECONDS.sleep(2);
      	if( count == 2  ){
				ssock.close();	
			}
			count++;
		}
   }
}
