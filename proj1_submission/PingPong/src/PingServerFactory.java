
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;

public class PingServerFactory implements java.io.Serializable
{

    String serverIP;
    int serverPort;

    PingServerFactory()
    {
        this(null, 0);
    }

    PingServerFactory(String IP, int port)
    {
        this.serverIP = IP;
        this.serverPort = port;
    }

    public <T> T makePingServer( Class<T> c ){
        System.out.println("Use Reflection to dynamically create object in a generic way");
        MyInvocationHandler handler = new MyInvocationHandler(this.serverIP, this.serverPort);

        T proxy = (T)Proxy.newProxyInstance(
                c.getClassLoader(),
                new Class[] { c },
                handler);
        return proxy;
    }

}
