
Version 1:

To run the multi-threaded TCP server, run:
	javac *.java
	java MultiThreadServer

To run the multi-thread TCP client, open another terminal tab and run:
	python client.py

The client issues a 100 threads and each thread represent a TCP client trying to connect to the server and send a random string to it. The client expects to get the same string but in uppercase back.

Version 2:

MultiThreadServer2.java starts a non-blocking listenning thread which handles all requests. To compile this new version, run:

javac worker.java listeningSocket.java MultiThreadServer2.java

Then run "python client.py" in another Terminal tab.

Version 3:

Another version of the server implemented. This version is closer to the real Skeleton server. To run adn test the example code, do the follow command to compile:

javac TestTCPServer.java MultiThreadServer3.java 

To run:

java TestTCPServer
