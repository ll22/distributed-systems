
import java.util.concurrent.TimeUnit;

public class TestTCPServer {

    public static void main(String args[]) throws Exception {

        MultiThreadServer3 test = new MultiThreadServer3();
        test.start();
        while (true) {
            System.out.println("main server running...");
            TimeUnit.SECONDS.sleep(1);
        }
    }
}