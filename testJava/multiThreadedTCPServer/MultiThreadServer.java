import java.io.IOException;
import java.io.DataOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class MultiThreadServer implements Runnable {
   Socket csocket;
   MultiThreadServer(Socket csocket) {
      this.csocket = csocket;
   }
   public static void main(String args[]) throws Exception {
      ServerSocket ssock = new ServerSocket(1234);
      System.out.println("Listening");

      //new thread when connected to realize multi-threading
      while (true) {
         Socket sock = ssock.accept();
         System.out.println("Connected");
         new Thread(new MultiThreadServer(sock)).start();
      }
   }
   public void run() {
      try {
         //PrintStream pstream = new PrintStream(csocket.getOutputStream());
         //for (int i = 100; i >= 0; i--) {
         //pstream.println(" bottles of beer on the wall");
         //}
         //pstream.close();
         BufferedReader inFromClient = new BufferedReader(new InputStreamReader(csocket.getInputStream()));
         DataOutputStream outToClient = new DataOutputStream(csocket.getOutputStream());
         String clientSentence = inFromClient.readLine();
         System.out.println("Received: " + clientSentence);
         outToClient.writeBytes(clientSentence.toUpperCase())
         outToClient.flush();
         inFromClient.close();
         //outToClient.close();
         //csocket.close();
         System.out.println("socket closed.");
      } catch (IOException e) {
         System.out.println(e);
      }
   }
}