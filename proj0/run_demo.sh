#bin/bash
docker create -v /tmp/ --name datacontainer datacontainer-image

docker run --rm -v $(pwd):/hostfiles --volumes-from datacontainer demo-image python hostfiles/catserver.py /tmp/test.txt 5005 &
sleep 0.5
docker run --rm -v $(pwd):/hostfiles -it --volumes-from datacontainer demo-image python hostfiles/catclient.py /tmp/test.txt 5005

