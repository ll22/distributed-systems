

# Output:
# 1. the total number of bigrams
# 2. the most common bigram
# 3. the number of bigrams required to add up to 10% of all bigrams

class BigramInfo(object):

    def __init__(self, name, freq):
        self.name = name
        self.freq = freq

    def __str__(self):
        return str(self.name + ' : ' + str(self.freq) )

    def __cmp__(self, other):
        return cmp(self.freq, other.freq)

    def __eq__(self, other):
        return self.freq == other.freq


if __name__ == "__main__":
  f = open("Bigram-result.txt", "r")
  totalBigramNum = 0
  maxFreq = 0
  maxBigram = BigramInfo('', 0)
  bigramInfos = []

  for line in f:
    # print line
    print str(totalBigramNum) + '\r',
    info = line.split(':')
    bigramInfo = BigramInfo( info[0], int( info[-1] ) )
    bigramInfos.append(bigramInfo)
    totalBigramNum = totalBigramNum + bigramInfo.freq
    if bigramInfo.freq > maxBigram.freq:
      maxBigram = bigramInfo

    bigramInfos.sort()

  # print results
  print 'Total Bigram Number: ' + str(totalBigramNum)
  print '\nMost Common Bigram: ' + str(maxBigram)
  print '\nNumber of bigrams required to add up to ten percent of all bigrams: '
  tenPercentNum = int(totalBigramNum * 0.1)
  sum = 0

  for bg in bigramInfos:
    sum = sum + bg.freq
    if sum <= tenPercentNum:
      print bg
    else:
      if sum - bg.freq == 0:
        print bg
      break

  '''
  print '\n\nAll Bigram counts are: '
  for bg in bigramInfos:
    print bg
  '''