#bin/bash

# cd /proj3

# create directories for bigram
hadoop fs -mkdir /user/cloudera/BigramExample
hadoop fs -mkdir /user/cloudera/BigramExample/input
# hadoop fs -mkdir /user/cloudera/BigramExample/output

# move file from local fs to hdfs input directory
hadoop fs -put /proj3/testWordCount.txt /user/cloudera/BigramExample/input

# check input
hadoop fs -ls /user/cloudera/BigramExample/input
# remove previous build directory
rm -rf build
mkdir -p build

# build
javac -cp /usr/lib/hadoop/*:/usr/lib/hadoop-mapreduce/* BigramExample.java -d build -Xlint
# generate jar file from directory build
jar -cvf BigramExample.jar -C build/ .

# switch identity to HDFS
su hdfs
#remove previous output
hadoop fs -rm -r -f /user/cloudera/BigramExample/output/
# change privilege
hadoop fs -chown hdfs /tmp/hadoop-yarn/staging/hdfs/.staging
# run application
hadoop jar /proj3/BigramExample.jar org.myorg.BigramExample /user/cloudera/BigramExample/input /user/cloudera/BigramExample/output
# check output
hadoop fs -cat /user/cloudera/BigramExample/output/*
