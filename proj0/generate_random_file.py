
import random

maxLineNum = 10000
lineNum = random.randint(0, maxLineNum)

def getRandNumStr():
    wordNum = 15
    wordRangeMax = 100
    wordRangeMin = 5
    line = ''
    for i in range(0, random.randint(wordNum-5, wordNum) ):
        line += str(random.randint(wordRangeMin, wordRangeMax)) 
    return line


for i in range(0, lineNum):
    print getRandNumStr()


