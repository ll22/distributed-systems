
import java.lang.Object;
import java.lang.reflect.Method;

import java.io.*;
import java.net.*;
import java.lang.ProcessBuilder;

class PingPongServer
{
   public static void main(String argv[]) throws Exception
      {
         String clientSentence;
         String capitalizedSentence;
         ServerSocket welcomeSocket = new ServerSocket(5678);

          server s = new server(); // create an object on which a method is going to be invoked

          // send PingServerFactory to client
          PingServerFactory factory = new PingServerFactory();
          Socket factorySocket = welcomeSocket.accept();
          ObjectOutputStream factoryOut = new ObjectOutputStream( factorySocket.getOutputStream() );
          factoryOut.flush();
          factoryOut.writeObject(factory);
          factoryOut.close();
          factorySocket.close();

         while(true)
         {
            Socket connectionSocket = welcomeSocket.accept();


            ObjectOutputStream out = new ObjectOutputStream( connectionSocket.getOutputStream() );
            out.flush();
            ObjectInputStream ois = new ObjectInputStream( connectionSocket.getInputStream() );

            // read the object which contains function names and list of argument
            transferContainer e = (transferContainer) ois.readObject();
            String funcName = e.funcName;

            System.out.println("function name: " + funcName); // print function name and function arguments
            Object returnValue = null;
            if(e.funcArgs != null){

              for(Object obj : e.funcArgs){
                System.out.println((int)obj);
              }

              //convert ArrayList to normal array
              Class [] paraTypes =  new Class[ e.parameterTypes.size() ];
              e.parameterTypes.toArray(paraTypes);

              Object [] arguments = new Object[ e.funcArgs.size() ];
              e.funcArgs.toArray(arguments);

              Class serverClass = server.class;
              Method methodToCall = serverClass.getMethod(funcName, paraTypes);
              returnValue = methodToCall.invoke(s, arguments);
            
            }else{

              Class serverClass = server.class;
              Method methodToCall = serverClass.getMethod(funcName);
              returnValue = methodToCall.invoke(s);
            }

            out.writeObject(returnValue);

            ois.close();
            out.close();
            connectionSocket.close();
         }
      }

}
