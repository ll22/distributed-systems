
import java.lang.Object;
import java.lang.reflect.Method;

import java.io.*;
import java.net.*;

class TCPClient
{
 public static void main(String argv[]) throws Exception
 {
  String sentence;
  String modifiedSentence;
  // create a client and connect to server
  Socket clientSocket = new Socket("localhost", 6789);
  ObjectOutputStream out = new ObjectOutputStream( clientSocket.getOutputStream() );

  // get the setNumer method from the Employee class
  Employee employee = new Employee();
  employee.name = "Lincong Li";
  employee.SSN = 12345;
  employee.address = "5215 Fiore Terrace";

  out.writeObject(employee);
  out.flush();
  out.close();

  clientSocket.close();
 }
}