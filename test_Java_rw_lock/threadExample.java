import java.util.concurrent.ThreadLocalRandom;

class PrintDemo {
   public void printCount(String threadName) {
      int randomNum;
      int min = 5;
      int max = 10;
      try {
         for(int i = 100; i > 0; i--) {
            System.out.println(threadName + " Counter   ---   "  + i );
            randomNum = ThreadLocalRandom.current().nextInt(min, max + 1);
            Thread.sleep(randomNum);
         }
      }catch (Exception e) {
         System.out.println(threadName + "Thread  interrupted.");
      }
   }
}

class ThreadDemo extends Thread {
   private Thread t;
   private String threadName;
   PrintDemo  PD;

   ThreadDemo( String name,  PrintDemo pd) {
      threadName = name;
      PD = pd;
   }
   
   public void run() {
      PD.printCount(threadName);
      System.out.println("Thread " +  threadName + " exiting.");
   }

   public void start () {
      System.out.println("Starting " +  threadName );
      if (t == null) {
         t = new Thread (this, threadName);
         t.start ();
      }
   }
}

public class threadExample {
   public static void main(String args[]) {

      PrintDemo PD = new PrintDemo();

      ThreadDemo T1 = new ThreadDemo( "Thread - 1 ", PD );
      ThreadDemo T2 = new ThreadDemo( "Thread - 2 ", PD );
      ThreadDemo T3 = new ThreadDemo( "Thread - 3 ", PD );
      ThreadDemo T4 = new ThreadDemo( "Thread - 4 ", PD );
      ThreadDemo T5 = new ThreadDemo( "Thread - 5 ", PD );

      T1.start();
      T2.start();
      T3.start();
      T4.start();
      T5.start();

      // wait for threads to end
      /*
      try {
         T1.join();
         T2.join();
      }catch( Exception e) {
         System.out.println("Interrupted");
      }
      */
   }
}

