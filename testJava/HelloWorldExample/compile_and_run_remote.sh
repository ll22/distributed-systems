#bin/bash

# compile all java files
javac -d $(pwd) Hello.java HelloImpl.java


# create the stub and skeleton for the HelloImpl remote object implementation
rmic HelloImpl

# start local registry on server
rmiregistry &

# Start the server
java  HelloImpl &
