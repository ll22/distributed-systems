
import java.io.IOException;
import java.io.DataOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class worker implements Runnable{

    Socket csocket;

    worker(Socket csocket){
        this.csocket = csocket;
    }

     public void run() {

      try {
         BufferedReader inFromClient = new BufferedReader(new InputStreamReader(csocket.getInputStream()));
         DataOutputStream outToClient = new DataOutputStream(csocket.getOutputStream());
         String clientSentence = inFromClient.readLine();
         System.out.println("Received: " + clientSentence);
         outToClient.writeBytes(clientSentence.toUpperCase());
         outToClient.flush();
         inFromClient.close();
         outToClient.close();
         csocket.close();
         System.out.println("socket closed.");
         System.out.println("Worker thread finished.");
      } catch (IOException e) {
         System.out.println(e);
      }

     }
}