
import java.lang.Object;
import java.lang.reflect.Method;

import java.io.*;
import java.net.*;

class reflectionServer
{
   public static void main(String argv[]) throws Exception
      {
         String clientSentence;
         String capitalizedSentence;
         ServerSocket welcomeSocket = new ServerSocket(6789);

         while(true)
         {
            Socket connectionSocket = welcomeSocket.accept();            
            ObjectInputStream ois = new ObjectInputStream( connectionSocket.getInputStream() );
            
            // read the object which contains function names and list of argument
            transferContainer e = (transferContainer) ois.readObject();
            //printMethodInfo(method);
            String funcName = e.funcName;

            // print function name and function arguments
            print(("function name: " + funcName) + "**\n" );
            if(e.funcArgs != null){

              for(Object obj : e.funcArgs){
                print( (String)obj + " " );
              }
              print("\n");

              //convert ArrayList to normal array
              Class [] paraTypes =  new Class[ e.parameterTypes.size() ];
              e.parameterTypes.toArray(paraTypes);
              System.out.println( "***** " + paraTypes[0].getSimpleName() );

              Object [] arguments = new Object[ e.funcArgs.size() ];
              e.funcArgs.toArray(arguments);

              // create an object on which a method is going to be invoked
              server s = new server();
              Class serverClass = server.class;
              Method methodToCall = serverClass.getMethod(funcName, paraTypes);
              methodToCall.invoke(s, arguments);
            
            }else{
              server s = new server();
              Class serverClass = server.class;
              Method methodToCall = serverClass.getMethod(funcName);
              methodToCall.invoke(s);
            }

            connectionSocket.close();
         }
      }

    private static void printMethodInfo(Method method){
        String methodCalled = method.getName();
        print( ("Method called: " + methodCalled) + "\n" );

        Class[] parameterTypes = method.getParameterTypes();
        Class[] exceptionTypes = method.getExceptionTypes();
        print("Input arguments are: \n");
        int argsNum = parameterTypes.length;
        for (int i = 0; i < argsNum; i++){
            print( ("Type: " + parameterTypes[i].getName()) + "\n");
        }

        print( "Exception type: ");
        for (Class exceptionType : exceptionTypes){
            print( exceptionType.getName() + "  " );
        }
        print("\n\n");
    }

    private static void print(String s){
        System.out.print(s);
    }

}