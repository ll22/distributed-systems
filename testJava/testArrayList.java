
import java.util.*;

public class testArrayList{

    public static void main(String args[]) {
        ArrayList al = new ArrayList();
        System.out.println("Contents of al: " + al);
        al.add(1);
        al.add(2);
        al.add(3);
        al.add(4);
        System.out.println("Contents of al: " + al);
        al.remove(3);
        System.out.println("Contents of al: " + al);
        al.remove(0);
        System.out.println("Contents of al: " + al);
    }
}


