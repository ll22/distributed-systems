
class Test(object):

    def __init__(self, attr1, attr2):
        self.attr1 = attr1
        self.attr2 = attr2

    def __str__(self):
        return str(self.__dict__)

    def __cmp__(self, other):
        return cmp(self.attr2, other.attr2)

    def __eq__(self, other):
        return self.attr2 == other.attr2

t1 = Test("foo", 42)
t2 = Test("foo", 43)
t3 = Test("barasdfafgergweff", 43)

print t1, t2, t3
print t1 == t2
print t2 == t3

print t2 > t1
print t2 >= t3
