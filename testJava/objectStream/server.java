
import java.lang.Object;
import java.lang.reflect.Method;

import java.io.*;
import java.net.*;

class TCPServer
{
   public static void main(String argv[]) throws Exception
      {
         String clientSentence;
         String capitalizedSentence;
         ServerSocket welcomeSocket = new ServerSocket(6789);

         while(true)
         {
            Socket connectionSocket = welcomeSocket.accept();            
            ObjectInputStream ois = new ObjectInputStream( connectionSocket.getInputStream() );
            Employee e = (Employee)ois.readObject();
            //printMethodInfo(method);
            print(e.name + "\n" );
            print(e.address + "\n" );
            print(e.SSN + "\n");
         }
      }

    private static void printMethodInfo(Method method){
        String methodCalled = method.getName();
        print( ("Method called: " + methodCalled) + "\n" );

        Class[] parameterTypes = method.getParameterTypes();
        Class[] exceptionTypes = method.getExceptionTypes();
        print("Input arguments are: \n");
        int argsNum = parameterTypes.length;
        for (int i = 0; i < argsNum; i++){
            print( ("Type: " + parameterTypes[i].getName()) + "\n");
        }

        print( "Exception type: ");
        for (Class exceptionType : exceptionTypes){
            print( exceptionType.getName() + "  " );
        }
        print("\n\n");
    }

    private static void print(String s){
        System.out.print(s);
    }

}