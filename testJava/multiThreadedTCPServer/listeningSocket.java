import java.io.IOException;
import java.io.DataOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class listeningSocket implements Runnable  {

   ServerSocket ssock;
   listeningSocket(ServerSocket s) throws Exception {
      //this.ssock = new ServerSocket(port);
   	this.ssock = s;
	}

   public void run() {

        try{
            while (true) {
                Socket sock = ssock.accept();
                System.out.println("Connected, and start a new worker thread");
                new Thread(new worker(sock)).start();
            }
        } catch (IOException e) {
         System.out.println(e);
        }
		System.out.println("Listenning thread terminated.");
   }

}
