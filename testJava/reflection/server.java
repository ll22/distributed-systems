
import java.io.*;

public class server implements ServerInterface
{
    int serverID;

    public server(){
        this(0);
    }

    public server(int ID){
        this.serverID = ID;
    }

    public int func1(String arg){
        System.out.println("func1:v1 is called with arg " + arg);
        return arg.length();
    }

    public int func1(String arg1, String arg2, String arg3, String arg4){
        System.out.println("func1:v2 is called with arg " + arg1);
        return arg1.length() + arg2.length() + arg3.length() + arg4.length();
    }

    public int func2(String arg){
        System.out.println("func2 is called with arg " + arg);
        return arg.length();
    }

    public int func3(String arg){
        System.out.println("func3 is called with arg " + arg);
        return arg.length();
    }

    public int func4(){
        System.out.println("func4 is called with no arg");
        return 4;
    }
}
