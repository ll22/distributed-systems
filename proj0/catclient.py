import socket
import time
import threading
import sys

TCP_IP = '172.17.0.2'
BUFFER_SIZE = 1024
NEXT_LINE_CMD = 'LINE\n'
CLIENT_HAPPY_CMD = 'OK\n'
CLIENT_UNHAPPY_CMD = 'MISSING\n'
OKBLUE = '\033[94m'
FAILRED = '\033[91m'
ENDC = '\033[0m'

LIFE_PERIOD = 30
REQUEST_INTERVAL = 3

quit = False

def printBlue(s):
    print OKBLUE + s + ENDC

def printRed(s):
    print FAILRED + s + ENDC


def countDownTimer():
    global quit
    time.sleep(LIFE_PERIOD)
    print 'Time up!'
    quit = True

if __name__ == '__main__':
    if len(sys.argv) is not 3:
        print 'wrong args'
    else:
        filePath = sys.argv[1]
        tcpPort = int(sys.argv[2])

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((TCP_IP, tcpPort))
    #f = open('/tmp/test.txt', 'r')
    f = open(filePath, 'r')
    # start timer counting down in another thread
    t = threading.Thread(target=countDownTimer)
    t.start()
    print 'Timer started. Run for ' + str(LIFE_PERIOD) + ' seconds...'

    while not quit:
        s.send(NEXT_LINE_CMD)
        nextLine = (f.readline()).upper()
        if len(nextLine) is 0:
            # reopen file
            f.close() 
            f = open(filePath, 'r')
            nextLine = (f.readline()).upper()

        data = s.recv(BUFFER_SIZE)
        if data == nextLine:
            s.send(CLIENT_HAPPY_CMD)
            printBlue('OK')
        else:
            s.send(CLIENT_UNHAPPY_CMD)
            printRed('MISSING')

        time.sleep(REQUEST_INTERVAL)

    f.close()
    s.close()

