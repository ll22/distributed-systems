import socket
import sys
import logging

BUFFER_SIZE = 1024
NEXT_LINE_CMD = 'LINE\n'
CLIENT_HAPPY_CMD = 'OK\n'
CLIENT_UNHAPPY_CMD = 'MISSING\n'
OKBLUE = '\033[94m'
FAILRED = '\033[91m'
ENDC = '\033[0m'


def printBlue(s):
    logging.debug(OKBLUE + s + ENDC)

def printRed(s):
    logging.debug(FAILRED + s + ENDC)


if __name__ == '__main__':

    if len(sys.argv) is not 3:
        print 'wrong args'
    else:
        filePath = sys.argv[1]
        tcpPort = int(sys.argv[2])

    logging.basicConfig(level=logging.INFO)
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('', tcpPort))
    s.listen(1)

    conn, addr = s.accept()
    f = open(filePath, 'r')
    while True:
        msg = conn.recv(BUFFER_SIZE)
        if not msg: break
        if msg == NEXT_LINE_CMD:
            line = (f.readline()).upper()
            if len(line) is 0:
                # reopen file
                f.close() 
                f = open(filePath, 'r')
                line = (f.readline()).upper()

            conn.send(line)
            logging.debug( 'Sent line: ' + line )

        elif msg == CLIENT_HAPPY_CMD:
            printBlue('Clien is happy')

        elif msg == CLIENT_UNHAPPY_CMD:
            printRed('Clinet is mad')

        else:
            logging.debug( 'Unknown message received: ' + msg )

    f.close()
    conn.close()
    logging.debug( 'Server quits' )

