#/bin/bash

cd buildimages/build_data_volume_images
docker build -t datacontainer-image .
#docker create -v /tmp/ --name datacontainer datacontainer-image # create data volume container

cd -
cd buildimages/build_demo_images
docker build -t demo-image .
