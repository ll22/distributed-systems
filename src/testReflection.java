
import java.lang.reflect.Proxy;
import java.lang.Throwable;

public class testReflection {
   public static void main(String[] args) throws Throwable{
      server s = new server(1);

      System.out.println("Call function on the server object itself");
      s.func1("Hi");
      s.func2("there");
      s.func3("Lincong");
      ServerInterface proxy = createStub();
      proxy.func1("I", "am", "Lincong", "Li");

      ServerInterface proxyGeneric = (ServerInterface) createStubGeneric(ServerInterface.class);
      proxyGeneric.func1("I", "am", "Zhen", "Lian");

      proxyGeneric.func3("SHIT!");
      proxyGeneric.func4();

   }


   public static ServerInterface createStub(){

      System.out.println("Use Reflection to dynamically create object");
      MyInvocationHandler handler = new MyInvocationHandler();
      
      ServerInterface proxy = (ServerInterface) Proxy.newProxyInstance(
                            ServerInterface.class.getClassLoader(),
                            new Class[] { ServerInterface.class },
                            handler);
      return proxy;
   }

   public static <T> T createStubGeneric( Class<T> c ){
      System.out.println("Use Reflection to dynamically create object in a generic way");
      MyInvocationHandler handler = new MyInvocationHandler();

      T proxy = (T)Proxy.newProxyInstance(
                            c.getClassLoader(),
                            new Class[] { c },
                            handler);
      return proxy;
   }

}


