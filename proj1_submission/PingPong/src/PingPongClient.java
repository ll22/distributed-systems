
import java.lang.reflect.Proxy;
import java.lang.Throwable;
import java.net.*;
import java.lang.Object;
import java.io.*;

public class PingPongClient {
   public static void main(String[] args) throws Throwable{

      String serverIP = "172.17.0.2";
      int serverPort = 5678;
      // get PingServerFactory from the server
      Socket clientSocket = new Socket(serverIP, serverPort);
      ObjectInputStream ois = new ObjectInputStream( clientSocket.getInputStream() );
      PingServerFactory factory = (PingServerFactory) ois.readObject();
      ois.close();
      clientSocket.close();
      // create proxy
      factory.serverIP = serverIP;
      factory.serverPort = serverPort;
      ServerInterface proxy = (ServerInterface) factory.makePingServer( ServerInterface.class );

      //ServerInterface proxy = (ServerInterface) PingServerFactory.makePingServer( ServerInterface.class );
      int correctCount = 0;
      int failtCount = 0;
      for(int i=0; i<4; i++){
         String response = proxy.Ping(i);
         String expectedResponse = "Pong" + Integer.toString(i);
         if( response.equals(expectedResponse) ){
            //System.out.println("Correct");
            correctCount++;
         }else{
            //System.out.print("Failed. expected Pong" + i);
            //System.out.println(" got "+ response);
            failtCount++;
         }

      }
      //System.out.println("Passed: " + correctCount);
      System.out.println("4 Tests Completed, " + failtCount +" Tests Failed" );

      //System.out.println("return value from  proxy.Ping() is: " + response);

   }

}


