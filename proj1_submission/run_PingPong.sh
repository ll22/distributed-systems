#bin/bash

# run PingPing server in Docker. Compile all Java files in Docker and start running the server
docker run --rm -v $(pwd)/PingPong/src/:/PingPong/ --name server 70416d047355 bash /PingPong/compile_and_run_server.sh &

# wait until all compilation finishes
sleep 2

# run PingPong client in Docker
docker run --rm -v $(pwd)/PingPong/src/:/PingPong/ --name client  70416d047355 bash /PingPong/run_client.sh
echo "Client finished"
echo "Trying to remove the Docker container with the server running"
rm ./PingPong/src/*.class
docker stop server
echo "PingPong server Docker container removed."
echo "Everything finished."

