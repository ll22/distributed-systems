import java.util.concurrent.ThreadLocalRandom;


class MyReadWriteLock{

  private int readers       = 0;
  private int writers       = 0;
  private int writeRequests = 0;

  //reader lock and unlock
  public synchronized void lockRead(){
    
    while(writers > 0 || writeRequests > 0){
      try{
         wait();

      } catch(InterruptedException e) {
         System.out.println("InterruptedException happened...");
         return;
      }
    } 
    readers++;
  }

  public synchronized void unlockRead(){
    readers--;
    notifyAll();
  }

  //writer lock and unlock
  public synchronized void lockWrite() {
    writeRequests++;

    while(readers > 0 || writers > 0){
      try{

         wait();

      }catch(InterruptedException e){
         System.out.println("InterruptedException happened...");
         return;
      }
    }

    writeRequests--;
    writers++;
  }

  public synchronized void unlockWrite(){
    writers--;
    notifyAll();
  }

  public synchronized int getReaderNum(){
   return this.readers;
  }
}


class file{
   //private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
   private final MyReadWriteLock rwl = new MyReadWriteLock();

   //final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
   //private int shareLockHolder = 0;

   public void fLock(String threadName, boolean isXLock){
      //int randomMiniSec;
      if(isXLock){ // write lock

         System.out.println(threadName + " trying to get X lock...");
         this.rwl.lockWrite();
         System.out.println(threadName + " got X lock!");
         
      }else{ //

         System.out.println(threadName + " trying to get S lock...");
         //this.rwl.readLock().lock();
         this.rwl.lockRead();
         System.out.println(threadName + " got S lock! "); //+ rwl.getReaderNum());

      }

   }

   public void fUnLock(String threadName, boolean isXLock){

      if(isXLock){ // write lock
         //this.rwl.writeLock().unlock();
         this.rwl.unlockWrite();
         System.out.println(threadName + " released X lock...");

      }else{
         //this.rwl.readLock().unlock();
         this.rwl.unlockRead();
         System.out.println(threadName + " released S lock..."); //+ rwl.getReaderNum());
      }
   }

}


class TestThread extends Thread {
   private Thread t;
   private String threadName;
   File F;

   TestThread( String name,  File f) {
      threadName = name;
      this.F = f;
   }
   
   public void run() {
      //PD.printCount(threadName);
      //System.out.println("Thread " +  threadName + " exiting.");
      int randomNum = 0;
      boolean isXLock = false;
      for(int i=0; i<10; i++){
         randomNum = ThreadLocalRandom.current().nextInt(10, 50);
         
         //randomly decide to get a exclusive lock or shared lock
         isXLock = randomNum % 8 <= 2; // either 1 or 0
         //randomly decide how many miniseconds to wait

         this.F.fLock(this.threadName, isXLock);

         System.out.println(this.threadName + " is delaying...");
         try{
            Thread.sleep(randomNum);
         }catch(InterruptedException e){
            System.out.println("InterruptedException happened");
         }

         this.F.fUnLock(this.threadName, isXLock);


      }

   }

   public void start () {
      System.out.println("Starting " +  threadName );
      if (t == null) {
         t = new Thread (this, threadName);
         t.start ();
      }
   }
}


public class testMyRwLock {

   public static void main(String args[]) {

      //PrintDemo PD = new PrintDemo();
      File f = new File();

      TestThread1 T1 = new TestThread1("Thread - 1 ", f);
      TestThread1 T2 = new TestThread1("Thread - 2 ", f);
      TestThread1 T3 = new TestThread1("Thread - 3 ", f);


      T1.start();
      T2.start();
      T3.start();
   }
}

