
This folder contains example code about how to use Java Reflection. The "server class" is server.java, and the main class in testReflection.java instantiate an object of the server class and pass object.class to the "create()" function in the Stub class, which returns a dynamically generated object that implements the interface specified in "ServerInterface.java".

After compile all Java files together, open another Terminal tab and run "java reflectionServer". In another Terminal tab, run "java testReflection". "testReflection" first creates dynamic proxy object and then invoke functions on it. When functions are invoked on it, the "invoke()" hanlder in the "MyInvocationHandler.java" is called, and the function name and arguements of the invoked function are serialized and sent to the server via TCP.

On the server side (reflectionServer.java), it received the object and deserialized it. Then it uses the function information to invoke the corresponding method on a local object.

This process is very similar to how RMI works. But a very simplified version. All files in this directory could serve as a good template for implementing the Skeleton and Stub classes for project 1
