import java.io.FileNotFoundException;

 public interface ServerInterface
{
    public int func1(String path) throws FileNotFoundException, RMIException;
    public int func1(String arg1, String arg2, String arg3, String arg4) throws FileNotFoundException, RMIException;
    public int func2(String path) throws FileNotFoundException, RMIException;
    public int func3(String path) throws FileNotFoundException, RMIException;
    public int func4() throws FileNotFoundException, RMIException;
}

