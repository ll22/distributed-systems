
import java.net.*;
import java.io.*;

public class TestTCPConnect{

    public static void main(String...args) {
    //    TestTCPServer testTCPServer = new TestTCPServer();
    //    testTCPServer.runTest();
    //    InetSocketAddress address = new InetSocketAddress(1321);
        long t0 = 0;
        try {
          System.out.println("try to connect");
          t0 = System.currentTimeMillis();
          Socket socket = new Socket();
          //InetAddress.getByName("127.0.0.1"), 
          InetSocketAddress address = new InetSocketAddress(7000);
          System.out.println("Trying to connect!");
          socket.connect(address);
        } catch (IOException e) {
          e.printStackTrace();
        }
        long t1 = System.currentTimeMillis();
        System.out.println(t1 - t0);
      }
}