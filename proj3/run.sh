

# start Docker
docker run -i -t -v /Users/lincongli/Desktop/graduate_school/UCSD/2017_Winter/distributed/projects/proj3/:/proj3 --name cdh_wc7 -d -p 9020:9020 -p 60070:60070 -p 60010:60010 -p 60020:60020 -p 60075:60075 -p 9030:9030 -p 9031:9031 -p 9032:9032 -p 9033:9033 -p 9088:9088 -p 9040:9040 -p 9042:9042 -p 10021:10021 -p 19889:19889 -p 11001:11001 karthicks/cdh5-docker:latest

docker exec -i -t CONTAINERID bash -l

cd /proj3

# create directories
hadoop fs -mkdir /user/cloudera/BigramExample
hadoop fs -mkdir /user/cloudera/BigramExample/input
# hadoop fs -mkdir /user/cloudera/BigramExample/output

# move file from local fs to hdfs
hadoop fs -put /proj3/testWordCount.txt /user/cloudera/BigramExample/input

# check input
hadoop fs -ls /user/cloudera/BigramExample/input

# remove output
# hadoop fs -rm -f -r /user/cloudera/BigramExample/output

# compile and generate Jar file
# javac -cp /usr/lib/hadoop/*:/usr/lib/hadoop-mapreduce/* WordCount.java -d build -Xlint
rm -rf build
mkdir -p build
javac -cp /usr/lib/hadoop/*:/usr/lib/hadoop-mapreduce/* BigramExample.java -d build -Xlint
jar -cvf BigramExample.jar -C build/ .


su hdfs
# change privilege
hadoop fs -chown hdfs /tmp/hadoop-yarn/staging/hdfs/.staging
# run application
hadoop jar /proj3/BigramExample.jar org.myorg.BigramExample /user/cloudera/BigramExample/input /user/cloudera/BigramExample/output

# check output
hadoop fs -cat /user/cloudera/BigramExample/output/*

