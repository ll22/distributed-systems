
import java.lang.Object;
import java.lang.reflect.Method;
import java.io.*;
import java.net.*;

public class MyInvocationHandler implements java.lang.reflect.InvocationHandler{

  // The proxy parameter passed to the invoke() method is the dynamic proxy object
  // implementing the interface. Most often you don't need this object.

  // The Method object passed into the invoke() method represents the method 
  // called on the interface the dynamic proxy implements.

  // The Object[] args array contains the parameter values passed to the
  // proxy when the method in the interface implemented was called.
  public Object invoke(Object proxy, Method method, Object[] args)
  throws Throwable {
    //do something "dynamic" 
    System.out.println("In the invoke method T.T\n");
    print("In the invoke method.\n");
    printMethodInfo(method, args);
    Socket clientSocket = new Socket("localhost", 6789);
    ObjectOutputStream out = new ObjectOutputStream( clientSocket.getOutputStream() );
    
    //get method information
    String methodCalled = method.getName();
    Class[] parameterTypes = method.getParameterTypes();
    
    //create an object which is to be serialized
    transferContainer container = new transferContainer(methodCalled, args, parameterTypes);
    //transferContainer container = new transferContainer(methodCalled, args);

    out.flush();
    out.writeObject(container);
    //out.close();
    //clientSocket.close();

    return new java.lang.Integer(5);
  }

  // helper functions
  private void printMethodInfo(Method method, Object[] args){
    String methodCalled = method.getName();
    print( ("Method called: " + methodCalled) + "\n" );

    Class[] parameterTypes = method.getParameterTypes();
    Class[] exceptionTypes = method.getExceptionTypes();
    print("Input arguments are: \n");
    int argsNum = parameterTypes.length;
    for (int i = 0; i < argsNum; i++){
        print( ("Type: " + parameterTypes[i].getName()) + "\n");
        print( ("Value: " + args[i].toString()) + "\n\n");
    }

    print( "Exception type: ");
    for (Class exceptionType : exceptionTypes){
        print( exceptionType.getName() + "  " );
    }
    print("\n\n");
  }

  private void print(String s){
    System.out.print(s);
  }

}