#bin/bash

# remove all previously generated class files
rm *.class

# compile all java files
javac -d $(pwd) *.java

# create the stub and skeleton for the HelloImpl remote object implementation
rmic HelloImpl

# start registry on server
rmiregistry &

# Start the server
java  HelloImpl & 

sleep 0.1

# Run the client
java -Djava.security.policy=client.policy HelloClient

rm *.class
