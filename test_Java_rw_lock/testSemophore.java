
import java.util.concurrent.Semaphore;

import java.util.concurrent.ThreadLocalRandom;


class MySemophore {

  final int PERMITS = Integer.MAX_VALUE;
  Semaphore semaphore = new Semaphore(PERMITS, true); //fairness FIFO

  public void lockRead() {
    try {
      semaphore.acquire(1);
      System.out.println("readers: " + (this.PERMITS - semaphore.availablePermits()));

    } catch (InterruptedException e) {
      System.out.println("InterruptedException");
    }
  }

  public void unlockRead() {
    //try{
    semaphore.release(1);
    System.out.println("readers: " + (this.PERMITS - semaphore.availablePermits()));
    // }catch(InterruptedException e){
    // System.out.println("InterruptedException");
    // }
  }

  //writer lock and unlock
  public void lockWrite() {
    try {
      semaphore.acquire(PERMITS);
    } catch (InterruptedException e) {
      System.out.println("InterruptedException");
    }
  }

  public void unlockWrite() {
    // try{
    semaphore.release(PERMITS);
    // }catch(InterruptedException e){
    // System.out.println("InterruptedException");
    // }
  }

}


class File {
  //private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
  private final MySemophore rwl = new MySemophore();

  //final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
  //private int shareLockHolder = 0;

  public void fLock(String threadName, boolean isXLock) {
    //int randomMiniSec;
    if (isXLock) { // write lock

      System.out.println(threadName + " trying to get X lock...");
      this.rwl.lockWrite();
      System.out.println(threadName + " got X lock!");

    } else { //

      System.out.println(threadName + " trying to get S lock...");
      //this.rwl.readLock().lock();
      this.rwl.lockRead();
      System.out.println(threadName + " got S lock! "); //+ rwl.getReaderNum());

    }

  }

  public void fUnLock(String threadName, boolean isXLock) {

    if (isXLock) { // write lock

      System.out.println(threadName + " releasing X lock...");
      this.rwl.unlockWrite();
      //System.out.println(threadName + " released X lock...");

    } else {

      System.out.println(threadName + " releasing S lock...");
      this.rwl.unlockRead();
      //System.out.println(threadName + " released S lock..."); //+ rwl.getReaderNum());
    }
  }

}


class TestThread1 extends Thread {
  private Thread t;
  private String threadName;
  File F;

  TestThread1(String name, File f) {
    threadName = name;
    this.F = f;
  }

  public void run() {
    //PD.printCount(threadName);
    //System.out.println("Thread " +  threadName + " exiting.");
    int randomNum = 0;
    boolean isXLock = false;
    for (int i = 0; i < 10; i++) {
      randomNum = ThreadLocalRandom.current().nextInt(10, 50);

      //randomly decide to get a exclusive lock or shared lock
      isXLock = randomNum % 8 <= 4; // either 1 or 0
      //randomly decide how many miniseconds to wait

      this.F.fLock(this.threadName, isXLock);

      System.out.println(this.threadName + " is delaying...");
      try {
        Thread.sleep(randomNum);
      } catch (InterruptedException e) {
        System.out.println("InterruptedException happened");
      }

      this.F.fUnLock(this.threadName, isXLock);


    }

  }

  public void start() {
    System.out.println("Starting " + threadName);
    if (t == null) {
      t = new Thread(this, threadName);
      t.start();
    }
  }
}


public class testSemophore {

  public static void main(String args[]) {

    //PrintDemo PD = new PrintDemo();
    File f = new File();

    TestThread1 T1 = new TestThread1("Thread - 1 ", f);
    TestThread1 T2 = new TestThread1("Thread - 2 ", f);
    TestThread1 T3 = new TestThread1("Thread - 3 ", f);


    T1.start();
    T2.start();
    T3.start();
  }
}