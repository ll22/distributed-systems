package naming;

import common.Path;
import storage.Command;
import storage.Storage;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Stores storage server's stub pair.
 */
public class StorageServerStore {
  private Map<Path, Set<StorageServerInfo>> pathToSS;
  private Map<StorageServerInfo, Set<Path>> ssToPath;
  private Set<StorageServerInfo> emptySS;

  public StorageServerStore() {
    pathToSS = new HashMap<>();
    ssToPath = new HashMap<>();
    emptySS = new HashSet<>();
  }

  public int size() {
    return ssToPath.size() + emptySS.size();
  }

  public boolean containsStorageServer(Storage clientStub, Command commandStub) {
    StorageServerInfo ssInfo = new StorageServerInfo(clientStub, commandStub);
    if (ssToPath.containsKey(ssInfo) || emptySS.contains(ssInfo)) {
      return true;
    }
    return false;
  }

  /**
   * @param path either file path or directory path
   * @return
   */
  public Set<StorageServerInfo> getStorageServerInfoSet(Path path) {
    if (!pathToSS.containsKey(path)) {
      return null;
    }
    return pathToSS.get(path);
  }

  public Set<Path> getPathSet(StorageServerInfo ssInfo) {
    if (!ssToPath.containsKey(ssInfo)) {
      return null;
    }
    return ssToPath.get(ssInfo);
  }

  public StorageServerInfo getRandomStorageServer() {
    int min = 0;
    int max = size();
    int indexToPick = ThreadLocalRandom.current().nextInt(min, max);
    Iterator<StorageServerInfo> iter = ssToPath.keySet().iterator();
    for (int i = 0; i < indexToPick; i++) {
      iter.next();
    }
    return iter.next();
  }

  public void addEmptyStorageServerInfo(StorageServerInfo ssInfo) {
    emptySS.add(ssInfo);
  }

  public void add(Path path, StorageServerInfo ssInfo) {
    if (emptySS.contains(ssInfo)) {
      emptySS.remove(ssInfo);
    }
    pathToSSAdd(path, ssInfo);
    ssToPathAdd(ssInfo, path);
  }

  public void add(Path path, Set<StorageServerInfo> ssInfos) {
    pathToSSAdd(path, ssInfos);
    for (StorageServerInfo ssInfo : ssInfos) {
      if (emptySS.contains(ssInfo)) {
        emptySS.remove(ssInfo);
      }
      ssToPathAdd(ssInfo, path);
    }
  }

  public void add(Set<Path> paths, StorageServerInfo ssInfo) {
    if (emptySS.contains(ssInfo)) {
      emptySS.remove(ssInfo);
    }
    for (Path path : paths) {
      pathToSSAdd(path, ssInfo);
    }
    ssToPathAdd(ssInfo, paths);
  }

  public void remove(Path path) {
    if (pathToSS.containsKey(path)) {
      Set<StorageServerInfo> ssInfos = pathToSS.get(path);
      pathToSS.remove(path);
      for (StorageServerInfo ssInfo : ssInfos) {
        if (ssToPath.containsKey(ssInfo) && ssToPath.get(ssInfo).contains(path)) {
          ssToPath.get(ssInfo).remove(path);
        }
      }
    }
  }

  public void remove(StorageServerInfo ssInfo) {
    if (emptySS.contains(ssInfo)) {
      emptySS.remove(ssInfo);
      return;
    }
    if (ssToPath.containsKey(ssInfo)) {
      Set<Path> paths = ssToPath.get(ssInfo);
      ssToPath.remove(ssInfo);
      for (Path path : paths) {
        if (pathToSS.containsKey(path) && pathToSS.get(path).contains(ssInfo)) {
          pathToSS.get(path).remove(ssInfo);
        }
      }
    }
  }

  public boolean ssExist(StorageServerInfo ssInfo){
    return ssToPath.containsKey(ssInfo);
  }

  private void ssToPathAdd(StorageServerInfo ssInfo, Path path) {
    if (ssToPath.containsKey(ssInfo)) {
      ssToPath.get(ssInfo).add(path);
    } else {
      Set<Path> set = new HashSet<>();
      set.add(path);
      ssToPath.put(ssInfo, set);
    }
  }

  private void ssToPathAdd(StorageServerInfo ssInfo, Set<Path> paths) {
    if (ssToPath.containsKey(ssInfo)) {
      ssToPath.get(ssInfo).addAll(paths);
    } else {
      ssToPath.put(ssInfo, new HashSet<>(paths));
    }
  }

  private void pathToSSAdd(Path path, StorageServerInfo ssInfo) {
    if (pathToSS.containsKey(path)) {
      pathToSS.get(path).add(ssInfo);
    } else {
      Set<StorageServerInfo> set = new HashSet<>();
      set.add(ssInfo);
      pathToSS.put(path, set);
    }
  }

  private void pathToSSAdd(Path path, Set<StorageServerInfo> ssInfos) {
    if (pathToSS.containsKey(path)) {
      pathToSS.get(path).addAll(ssInfos);
    } else {
      pathToSS.put(path, new HashSet<>(ssInfos));
    }
  }

  public Set<StorageServerInfo> getEmptySS() {
    return emptySS;
  }

  public Map<StorageServerInfo, Set<Path>> getSsToPath() {
    return ssToPath;
  }

  public List<StorageServerInfo> getHostingServerInfoList() {
    List<StorageServerInfo> ret = new ArrayList<>(ssToPath.size());
    for (StorageServerInfo ssInfo : ssToPath.keySet()) {
      ret.add(ssInfo);
    }
    return ret;
  }

  public Set<StorageServerInfo> getHostingServerInfoSetForDir(Path dirPath) {
    Set<StorageServerInfo> hostingServerSet = new HashSet<>();
    for (Map.Entry<Path, Set<StorageServerInfo>> passToSSEntry : pathToSS.entrySet()) {
      if (passToSSEntry.getKey().getAbsolutePath().startsWith(dirPath.getAbsolutePath())) {
        hostingServerSet.addAll(passToSSEntry.getValue());
      }
    }
    return hostingServerSet;
  }
}
