import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.locks.*;


class file{
   private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
   //final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
   private int shareLockHolder = 0;

   public void fLock(String threadName, boolean isXLock){
      //int randomMiniSec;
      if(isXLock){ // write lock

         System.out.println(threadName + " trying to get X lock...");
         this.rwl.writeLock().lock();
         System.out.println(threadName + " got X lock!");
         
      }else{ //

         System.out.println(threadName + " trying to get S lock...");
         this.rwl.readLock().lock();
         this.shareLockHolder++;
         System.out.println(threadName + " got S lock! " + this.shareLockHolder);

      }

   }

   public void fUnLock(String threadName, boolean isXLock){

      if(isXLock){ // write lock
         System.out.println(threadName + " releasing X lock...");
         this.rwl.writeLock().unlock();

      }else{
         this.shareLockHolder--;
         System.out.println(threadName + " releasing S lock..." + shareLockHolder);
         this.rwl.readLock().unlock();
      }
   }

}


class TestThread extends Thread {
   private Thread t;
   private String threadName;
   File F;

   TestThread( String name,  File f) {
      threadName = name;
      this.F = f;
   }
   
   public void run() {
      //PD.printCount(threadName);
      //System.out.println("Thread " +  threadName + " exiting.");
      int randomNum = 0;
      boolean isXLock = false;
      for(int i=0; i<200; i++){
         randomNum = ThreadLocalRandom.current().nextInt(10, 50);
         
         //randomly decide to get a exclusive lock or shared lock
         isXLock = randomNum % 8 <= 2; // either 1 or 0
         //randomly decide how many miniseconds to wait

         this.F.fLock(this.threadName, isXLock);

         System.out.println(this.threadName + " is delaying...");
         try{
            Thread.sleep(randomNum);
         }catch(InterruptedException e){
            System.out.println("InterruptedException happened");
         }

         this.F.fUnLock(this.threadName, isXLock);


      }

   }

   public void start () {
      System.out.println("Starting " +  threadName );
      if (t == null) {
         t = new Thread (this, threadName);
         t.start ();
      }
   }
}


public class testRwLock {

/*
   class File{
      private final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
      //final ReentrantReadWriteLock rwl = new ReentrantReadWriteLock();
      private int shareLockHolder = 0;

      public void fLock(String threadName, int lockType){
         //int randomMiniSec;
         if(lockType == 1){ // write lock

            System.out.println(threadName + " trying to get X lock...");
            this.rwl.writeLock().lock();
            System.out.println(threadName + " got X lock!");
            
         }else{ //

            System.out.println(threadName + " trying to get S lock...");
            this.rwl.readLock().lock();
            this.shareLockHolder++;
            System.out.print(threadName + " got S lock! " + this.shareLockHolder);

         }

      }

      public void fUnLock(String threadName, int lockType){

         if(lockType == 1){ // write lock
            System.out.println(threadName + " releasing X lock...");
            this.rwl.writeLock().unlock();

         }else{
            this.shareLockHolder--;
            System.out.println(threadName + " releasing S lock...");
            this.rwl.readLock().unlock();
         }
      }

   }


   class testThread extends Thread {
      private Thread t;
      private String threadName;
      File F;

      testThread( String name,  File f) {
         threadName = name;
         this.F = f;
      }
      
      public void run() {
         //PD.printCount(threadName);
         //System.out.println("Thread " +  threadName + " exiting.");
         int randomNum = 0;
         int randomLockType = 1;
         for(int i=0; i<100; i++){
            randomNum = ThreadLocalRandom.current().nextInt(5, 100);
            
            //randomly decide to get a exclusive lock or shared lock
            randomLockType = randomNum % 2; // either 1 or 0
            //randomly decide how many miniseconds to wait

            this.F.fLock(this.threadName, randomLockType);

            System.out.println(this.threadName + " is delaying...");
            Thread.sleep(randomNum);
            this.F.fUnLock(this.threadName, randomLockType);


         }

      }

      public void start () {
         System.out.println("Starting " +  threadName );
         if (t == null) {
            t = new Thread (this, threadName);
            t.start ();
         }
      }
   }
*/

   public static void main(String args[]) {

      //PrintDemo PD = new PrintDemo();
      File f = new File();

      TestThread1 T1 = new TestThread1("Thread - 1 ", f);
      TestThread1 T2 = new TestThread1("Thread - 2 ", f);
      TestThread1 T3 = new TestThread1("Thread - 3 ", f);


      T1.start();
      T2.start();
      T3.start();
   }
}

