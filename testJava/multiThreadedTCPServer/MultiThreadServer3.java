import java.io.IOException;
import java.io.DataOutputStream;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.TimeUnit;

public class MultiThreadServer3 {

   //ServerSocket ssock;
   private static int aliveThread;
   private static final Object countLock = new Object();

   MultiThreadServer3(){
      this.aliveThread = 0;
   }

   public void start() throws Exception {

      ServerSocket ssock = new ServerSocket(1234);
      System.out.println("Listenning thread started");
      Thread t = new Thread(new listeningSocket( ssock ));
      t.start();
   }


   private class listeningSocket implements Runnable{

      ServerSocket ssock;
      listeningSocket(ServerSocket s) throws Exception {
         this.ssock = s;
      }

      public void run() {

           try{
               while (true) {
                   Socket sock = ssock.accept();
                   System.out.println("Connected, and start a new worker thread");
                   new Thread(new worker(sock)).start();
               }
           } catch (IOException e) {
            System.out.println(e);
           }
      }
   }

   private class worker implements Runnable{

       Socket csocket;

       worker(Socket csocket){
           this.csocket = csocket;
       }

        public void run() {
         increaseThreadCounter();
         System.out.println("Thread created, current number of threads: " + aliveThread);
         try {
            BufferedReader inFromClient = new BufferedReader(new InputStreamReader(csocket.getInputStream()));
            DataOutputStream outToClient = new DataOutputStream(csocket.getOutputStream());
            String clientSentence = inFromClient.readLine();
            System.out.println("Received: " + clientSentence);
            outToClient.writeBytes(clientSentence.toUpperCase());
            outToClient.flush();
            inFromClient.close();
            outToClient.close();
            csocket.close();
            System.out.println("socket closed.");
            System.out.println("Worker thread finished.");
         } catch (IOException e) {
            System.out.println(e);
         }
         decreaseThreadCounter();
         System.out.println("Thread died, current number of threads: " + aliveThread);
        }


        private void increaseThreadCounter(){
            synchronized (countLock) {
               aliveThread++;
            }
        }

        private void decreaseThreadCounter(){
            synchronized (countLock) {
               aliveThread--;
            }
        }

   }

}
