package naming.fs;

//import com.c05mic.generictree.Node;
//import com.c05mic.generictree.Tree;

import common.Path;

import java.io.FileNotFoundException;
import java.util.*;

/**
 * Represents the distributed file system.
 * Use Map<String, FileInfo> + Tree to achieve O(n) memory cost and O(1) access, n is the number of file and
 * directories stored in DFS.
 */
public class FileSystem {
  Tree fs;
  Map<String, Node<FileInfo>> fsMap;

  public FileSystem() {
    init();
  }

  public void init() {
    fs = new Tree(new Node<FileInfo>(new FileInfo(true))); // root / is directory
    fsMap = new HashMap<>();
    addFileOrDir("", true, null); // add the above root to filesystem
  }

  public boolean isDirectory(Path path) throws FileNotFoundException {
    if (!fsMap.containsKey(path.getAbsolutePath())) {
      throw new FileNotFoundException("Path " + path.getAbsolutePath() + " does not exist.");
    }
    return fsMap.get(path.getAbsolutePath()).getData().isDirectory;
  }

  // checks if the file exists
  public boolean containsFile(Path filePath) {
    if (!fsMap.containsKey(filePath.getAbsolutePath())) {
      return false;
    }
    return true;
  }

  // List everything under this directory, notice this is not recursive since UNIX handles this non-recursively.
  public String[] list(Path dirPath) throws FileNotFoundException {
    return list(dirPath, false, false);
  }

  // Similar to the previous method, but does so recursively
  public String[] listRecursively(Path dirPath) throws FileNotFoundException {
    return list(dirPath, true, true);
  }

  private String[] list(Path dirPath, boolean isRecursive, boolean isPath) throws FileNotFoundException {
    if (!fsMap.containsKey(dirPath.getAbsolutePath())) {
      throw new FileNotFoundException("Directory " + dirPath.getAbsolutePath() + " does not exist.");
    }
    Node thisDir = fsMap.get(dirPath.getAbsolutePath());
    List<Node<FileInfo>> childrenOfThisDir;
    if (isRecursive) {
      childrenOfThisDir = getAllChildren(thisDir);
    } else {
      childrenOfThisDir = thisDir.getChildren();
    }
    String[] paths = new String[childrenOfThisDir.size()];
    for (int i = 0; i < paths.length; i++) {
      if (isPath) {
        paths[i] = dirPath + "/" + childrenOfThisDir.get(i).getData().name; // path instead of just a name
      } else {
        paths[i] = childrenOfThisDir.get(i).getData().name; // just a name
      }
    }
    return paths;
  }

  // create a file given filePath
  public boolean createFile(Path filePath) throws FileNotFoundException {
    if (fsMap.containsKey(filePath)) {
      return false;
    }
    Path parentPath = filePath.parent();
    if (!fsMap.containsKey(parentPath.getAbsolutePath())) {
      throw new FileNotFoundException("Directory " + parentPath.getAbsolutePath() + " does not exist.");
    }
    Node<FileInfo> parent = fsMap.get(parentPath.getAbsolutePath());
    addFileOrDir(filePath.getAbsolutePath(), false, parent);
    return true;
  }

  // create a directory given dirPath
  public boolean createDirectory(Path dirPath) throws FileNotFoundException {
    if (containsFile(dirPath)) {
      return false;
    }
    Path parentOfDir = dirPath.parent();
    if (!fsMap.containsKey(parentOfDir.getAbsolutePath()) || !getFileInfo(parentOfDir.getAbsolutePath()).isDirectory()) {
      throw new FileNotFoundException("Directory " + parentOfDir.getAbsolutePath() + " does not exist.");
    }
    Node<FileInfo> parent = fsMap.get(parentOfDir.getAbsolutePath());
    addFileOrDir(dirPath.getAbsolutePath(), true, parent);
    return true;
  }

  // delete a file or dir
  public boolean delete(Path path) throws FileNotFoundException {
    Path parentPath = path.parent();
    if (!fsMap.containsKey(path.getAbsolutePath())) {
      throw new FileNotFoundException("Directory " + parentPath.getAbsolutePath() + " does not exist.");
    }
    Node<FileInfo> parent = fsMap.get(parentPath.getAbsolutePath());
    removeFileOrDir(path.getAbsolutePath(), parent);
    return true;
  }

  /**
   * Add file/dir to filesystem
   *
   * @param path   absolute path to this file/dir
   * @param isDir  true if is directory
   * @param parent the parent node of this new file/dir
   */
  private void addFileOrDir(String path, boolean isDir, Node<FileInfo> parent) {
    FileInfo fileInfo = new FileInfo(isDir);
    fileInfo.name = getName(path);
    Node<FileInfo> node = new Node<>(fileInfo);
    fsMap.put(path, node);
    if (parent != null) { // if parent == null we are adding root
      parent.addChild(node);
    }
  }

  // same as above + assume all operation is valid: path is valid, parent is valid
  private void removeFileOrDir(String path, Node<FileInfo> parent) {
    if (parent == null) { // remove the entire filesystem
      init();
    }
    Node<FileInfo> fileOrDirToRemove = fsMap.get(path);
    fsMap.remove(path);
    parent.removeChild(fileOrDirToRemove);
  }

  // get all children of this node, yes, recursively
  public List<Node<FileInfo>> getAllChildren(Node<FileInfo> node) {
    List<Node<FileInfo>> allChildren = new ArrayList<>();
    DFS(node, allChildren);
    return allChildren;
  }

  // very inefficient O(n), don't use unless have to
  // get all !FILE! paths under dirPath recursively
  public List<String> getAllChildFilePaths(Path dirPath) {
    List<String> childPaths = new ArrayList<>();
    List<Node<FileInfo>> childNodes = new ArrayList<>();
    DFS(fsMap.get(dirPath.getAbsolutePath()), childNodes);
    HashSet<Node<FileInfo>> childNodeSet = new HashSet<>(childNodes);
    for (Map.Entry<String, Node<FileInfo>> fsMapEntry : fsMap.entrySet()) {
      if (childNodeSet.contains(fsMapEntry.getValue()) && !fsMapEntry.getValue().getData().isDirectory()) { // file only
        childPaths.add(fsMapEntry.getKey());
      }
    }
    return childPaths;
  }

  // depth first search used to get all nodes under a node
  private void DFS(Node<FileInfo> node, List<Node<FileInfo>> results) {
    if (node.getChildren() != null) {
      results.addAll(node.getChildren());
      for (Node<FileInfo> child : node.getChildren()) {
        DFS(child, results);
      }
    }
  }

  // get the last component of a path
  private String getName(String path) {
    if (path.length() == 0) {
      return "";
    }
    List<String> components = Path.getComponents(path);
    return components.get(components.size() - 1);
  }

  // get FileInfo object from file system given filePath
  public FileInfo getFileInfo(String filePath) {
    if (fsMap.containsKey(filePath)) {
      return fsMap.get(filePath).getData();
    }
    return null;
  }
}
