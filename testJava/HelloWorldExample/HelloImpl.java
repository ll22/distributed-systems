import java.rmi.Naming; 
import java.rmi.RemoteException; 
import java.rmi.server.UnicastRemoteObject;
public class HelloImpl extends UnicastRemoteObject implements Hello 
{ 
    public HelloImpl() throws RemoteException {}

    public String sayHello() { 
	System.out.println("remote object method (sayHello) accessed ");
	return "Hello world!"; 
    }

    public static void main(String args[]) 
    { 
	//System.setProperty("java.security.policy","file:/testJava/HelloWorldExample/client.policy");
        try 
        { 
            HelloImpl obj = new HelloImpl(); 
            // Bind this object instance to the name "HelloServer" 
            Naming.rebind("HelloServer", obj); 
        }
        catch (Exception e) 
        { 
            System.out.println("HelloImpl err: " + e.getMessage()); 
            e.printStackTrace(); 
        } 
    } 
}
