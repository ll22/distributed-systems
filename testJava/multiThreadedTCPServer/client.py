
import socket
import sys
import threading
import random
import string
import time

TCP_IP = '127.0.0.1'
TCP_PORT = 1234
BUFFER_SIZE = 1024

def id_generator(size=120, chars=string.ascii_lowercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def delay_random_sec(max_delay_sec):
    random_dely_sec = random.randint(1,max_delay_sec*10) / 10.0
    time.sleep(random_dely_sec)

def TCP_client():

    delay_random_sec(1)
    # create socket and connect
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((TCP_IP, TCP_PORT))

    # delay random seconds 
    delay_random_sec(1)

    # generate and send random string
    random_str = id_generator() + '\n'
    s.send(random_str)
    #print 'sent'. receive data until no data
    recev_msg = ''
    recev_data = s.recv(BUFFER_SIZE)
    while recev_data:
        recev_msg += recev_data
        recev_data = s.recv(BUFFER_SIZE)

    #[:-1] it returns all elements [:] except the last one
    if str.upper(random_str)[:-1] != recev_msg:
        print 'F'
    else:
        print 'P'
    s.close()

if __name__ == "__main__":
    thread_num = 100 # if this number is too big. "Too many open files" error would occur when trying to create socket in a thread
    threads = []
    for i in range(thread_num):
        t = threading.Thread(target=TCP_client)
        threads.append(t)
        t.start()

